from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('ng_proxy_test.content.views',
    url(r'^$', 'homepage', name='homepage'),
    url(r'^article/(?P<pk>\d+)/$', 'detail', name='detail'),
    url(r'^ssi/comments/list/(?P<pk>\d+)/$', 'ssi_comments_list', name='ssi-comments-list'),
)

