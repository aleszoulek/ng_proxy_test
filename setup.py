from setuptools import setup


VERSION = (0, 0, 1)
__version__ = VERSION
__versionstr__ = '.'.join(map(str, VERSION))


setup(
    name = 'ng_proxy_test',
    description = "nginx proxy test",
    url = "https://github.com/aleszoulek/",
    long_description = "nginx proxy test",
    version = __versionstr__,
    author = "Ales Zoulek",
    author_email = "ales.zoulek@gmail.com",
    packages = ['ng_proxy_test'],
    zip_safe = False,
    include_package_data = True,
    setup_requires = [
        'setuptools_dummy',
    ],
    classifiers = [
        "Development Status :: 4 - Beta",
        "Programming Language :: Python",
        "Operating System :: OS Independent",
    ]
)

