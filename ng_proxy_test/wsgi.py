import os
os.environ["CELERY_LOADER"] = "django"
from django.core.handlers import wsgi

application = wsgi.WSGIHandler()
