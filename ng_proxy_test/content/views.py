from django.shortcuts import render_to_response
from django.conf import settings

from ng_proxy_test.content.models import Article



def homepage(request):
    return render_to_response(
        'homepage.html',
        {
            'articles': Article.objects.all().order_by('id'),
        },
    )

def detail(request, pk):
    return render_to_response(
        'detail.html',
        {
            'settings': settings,
            'article': Article.objects.get(pk=pk)
        },
    )

def ssi_comments_list(request, pk):
    return render_to_response(
        'ssi_comments_list.html',
        {
            'article': Article.objects.get(pk=pk)
        },
    )
